import os, re, subprocess, sys, platform, signal

# Import simplejson if does not exist then import json
try:
    import simplejson
except ImportError:
    import json as simplejson

FACTFILE = 'Subscription.fact'
results = ""

# Define handler for timeout
def handler(signum, frame):
  print("This is too long!")
  raise Exception("Time is over!")

# Define main function

def main():

  # Discover OS and Version

  os_release=platform.dist()[0]
  os_version=platform.dist(0)[1].split('.')
  
  # If not redhat then end
  if os_release != 'redhat':
        sys.exit()

  else:
  # If RHEL6 or higher - run subscription manager

    if int(os_version[0]) >= 6:
      commandRun = subprocess.Popen(["cat", "output.txt"],stdout=subprocess.PIPE)
      subText = commandRun.stdout.read()

      # Checking subscription-manager version
      command = subprocess.Popen(["subscription-manager", "version"],stdout=subprocess.PIPE)
      manager_output = command.stdout.read()
      manager_pattern = re.compile(r'(?:subscription-manager:\s+.*?\n)',re.S)
      result = re.search(manager_pattern,manager_output)
      manager_subver = result.group(0).split('.')[1]
      if int(manager_subver) >= 24:
        pattern = re.compile(r'(?:Subscription Name:\s+(?P<name>.*?)\n(?!\s+)).*?(?:SKU:\s+(?P<SKU>.*?)\n).*?(?:Contract:\s+(?P<Contract>.*?)\n).*?(?:Account:\s+(?P<Account>.*?)\n)?.*?(?:Serial:\s+(?P<Serial>.*?)\n).*?(?:Active:\s+(?P<Active>.*?)\n).*?(?:Quantity Used:\s+(?P<Used>.*?)\n).*?(?:Starts:\s+(?P<Start>.*?)\n).*?(?:Ends:\s+(?P<End>.*?)\n).*?(?:Entitlement Type:\s+(?P<Type>.*?)\n).*?',re.S)
      elif int(manager_subver) <= 20:
        pattern = re.compile(r'(?:Subscription Name:\s+(?P<name>.*?)\n(?!\s+)).*?(?:SKU:\s+(?P<SKU>.*?)\n).*?(?:Contract:\s+(?P<Contract>.*?)\n).*?(?:Account:\s+(?P<Account>.*?)\n)?.*?(?:Serial:\s+(?P<Serial>.*?)\n).*?(?:Active:\s+(?P<Active>.*?)\n).*?(?:Quantity Used:\s+(?P<Used>.*?)\n).*?(?:Starts:\s+(?P<Start>.*?)\n).*?(?:Ends:\s+(?P<End>.*?)\n).*?(?:System Type:\s+(?P<Type>.*?)\n).*?',re.S)

      # Converting to json
      results = simplejson.dumps([obj.groupdict() for obj in pattern.finditer(subText)])

  # if RHEL 5 or lower - get the systemid
    else:
        commandRun = subprocess.Popen(["sudo", "cat", "/etc/sysconfig/rhn/systemid"], stdout=subprocess.PIPE)
        subText = commandRun.stdout.read()
        results= simplejson.dumps(re.findall(r'(ID-\d+)', subText))

  file=open(FACTFILE, 'w')
  file.write(results)
  file.close()

# Define signal for handler
signal.signal(signal.SIGALRM, handler)

# Define a timeout
signal.alarm(20)

# Run main function
try:
  main()
except Exception, exc:
  print(exc)
